#!/bin/bash


CFT=$(ls | grep SOA | wc -l)

chemin=$(pwd)

if [ $CFT -eq 1 ]
then

version=$1

appli=$(ls | grep CFT | cut -b 1-3)

specific=$(ls | grep CFT | cut -b 9-15)

cible=$(ls | grep CFT)
exploit=${cible}/src/CFTDIRHOME_SISTER/exploit

DPKGL=DPKGL_${appli}_${version}_SIS_${specific}_US
LAP=PKG_LAP_${appli}_SIS_${specific}_US_${version}
DOC=PKG_DOC_${appli}_SIS_${specific}_US_${version}

mkdir tmp
mkdir doc

cp -r ${cible}/doc/* doc

cp -r ${cible}/src/CFTDIRHOME_SISTER/exploit tmp

awk '{ sub("\r$", ""); print }' ${cible}/src/CFT_SIS_${specific}_US.mc  >  tmp/CFT_SIS_${specific}_US_${version}.mc
echo "VERSION=${version}" >> tmp/CFT_SIS_${specific}_US_${version}.mc


execrf=$(ls ${exploit} | awk '{print $1}' | grep execrf | wc -l)

if [ $execrf -eq 1 ]
then
	ls ${exploit}/execrf | awk '{print $1}'  > list
for line in $(cat list);
do
        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/execrf" >> tmp/${DPKGL}   ;
done

fi

execsf=$(ls ${exploit} | awk '{print $1}' | grep execsf | wc -l)

if [ $execsf -eq 1 ]
then
        
         ls ${exploit}/execsf | awk '{print $1}'  > list
for line in $(cat list);
do
        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/execsf" >> tmp/${DPKGL}   ;
done

fi

execse=$(ls ${exploit} | awk '{print $1}' | grep execse | wc -l)

if [ $execsf -eq 1 ]
then

         ls ${exploit}/execse | awk '{print $1}'  > list
for line in $(cat list);
do
        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/execse" >> tmp/${DPKGL}  ;
done

fi

script=$(ls ${exploit} | awk '{print $1}' | grep script | wc -l)

if [ $script -eq 1 ]
then

         ls -l ${exploit}/script | grep ^- | awk '{print $9}'  > list
for line in $(cat list);
do
        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/script" >> tmp/${DPKGL}   ;
done
         ls ${exploit}/script/recept | grep . > list

	 for line in $(cat list)
		 do
			 echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/script/recept" >> tmp/${DPKGL}   ;
		 done

         ls ${exploit}/script/send | grep . > list

         for line in $(cat list)
                 do
                         echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/script/send" >> tmp/${DPKGL}  ;
                 done
fi


config=$(ls ${exploit} | awk '{print $1}' | grep config | wc -l)

if [ $config -eq 1 ]
then 
	ls ${exploit}/config/I26TX | grep . > list
	for line in $(cat list)
               do
                        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/config/I26TX" >> tmp/${DPKGL}   ;
               done

	       ls ${exploit}/config/IN6TX | grep . > list
        for line in $(cat list)
               do
                        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/config/IN6TX" >> tmp/${DPKGL}   ;
               done


	       ls ${exploit}/config/OP6TX | grep . > list
        for line in $(cat list)
               do
                        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/config/OP6TX" >> tmp/${DPKGL}   ;
               done

	       
 		ls ${exploit}/config/UA6TX | grep . > list
        for line in $(cat list)
               do
                        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/config/UA6TX" >> tmp/${DPKGL}   ;
               done
fi

execre=$(ls ${exploit} | awk '{print $1}' | grep execre | wc -l)

if [ $execre -eq 1 ]
then

         ls ${exploit}/execre | awk '{print $1}'  > list
for line in $(cat list);
do
        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/execre" >> tmp/${DPKGL}   ;
done

fi

transco=$(ls ${exploit} | awk '{print $1}' | grep transco | wc -l)

if [ $transco -eq 1 ]
then

         ls ${exploit}/transco | awk '{print $1}'  > list
for line in $(cat list);
do
        echo "APP $line 0 ""$""CFTDIRHOME_SISTER exploit/transco" >> tmp/${DPKGL}   ;
done

fi

cd tmp
tar -czf ${LAP}.tar.gz *
mv ${LAP}.tar.gz ${chemin}
cd ${chemin}/doc
zip -r ${DOC}.zip *
mv ${DOC}.zip ${chemin}
cd ..
rm -rf tmp
rm -rf list
rm -rf doc

fi
